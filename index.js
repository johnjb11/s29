
// require DIRECTIVE tells us to load the express module
const express = require('express');


// creating a server using express
const app = express ();


//port
const port = 4000;



//middlewares
app.use(express.json())


// allows to read a json data
app.use(express.urlencoded({extended:true}));

// allows app to read data from forms
// by default, information received from the url can only be received as string or an array
// with extended: true, this allows to receive information in other data types such as objects


//mock database

let users = [
	{
		email: "nezukoKamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tanjiroKamado@gmail.com",
		username: "gonpanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}


]

let loggedUser;

app.get('/', (req, res) =>{
	res.send('Hello World')
});


//app = server
// get - HTTP method
// '/' = route name or endpoint
// (req, res) = request and response - will handle the requests and the responses = you can change req and res words but action will still be request and response
// res.send - combines writeHead() and end(), used to send response to our client


app.get('/hello', (req, res) =>{
	res.send('Hello from Batch 131')
});



//post method

//postman, post, raw, json, then the output in body will appear at bash


app.post('/', (req,res) => {
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age} years old. I could be described as ${req.body.description}`)

})


//register route
app.post('/users/register', (req,res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username:req.body.username,
		password:req.body.password,
		isAdmin:req.body.isAdmin,
	}

	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered.`)
})	



//login route

app.post('/users/login', (req,res) => {
	console.log(req.body); /*console.log just to check or debug the codes, not revealed at final product*/

	let foundUser = users.find((user)=>{

		return user.username === req.body.username && user.password === req.body.password;
	});

	if(foundUser !== undefined){

		let foundUserIndex = users.findIndex((user)=>{

			return user.username === foundUser.username

		})

		foundUser.index = foundUserIndex;

		loggedUser = foundUser;

		console.log(loggedUser);

		res.send('Thank you for loggin in')
	} else {

		loggedUser = foundUser;

		res.send('Login Failed, wrong credentials.')
	}

})


//Change-Password Route

app.put('/users/change-password', (req,res) => {

	//store the message that will be sent back to our client
	let message;

	//will loop through all the the "users' array"
	for(let i = 0; i < users.length; i++){

		//if the username provided in the request is the same with the username in the loop
		if(req.body.username === users[i].username){

			//change the password of the user found in the loop by the requested password in the body of the client
			users[i].password = req.body.password;


			//send a message to the client
			message = `user ${req.body.username}'s password has been changed.`;

			break;
		} else {

			message = `User not found.`
		}
	}

	res.send(message)
})



app.get('/home', (req,res) => {
	res.send(`simple`)
})


app.get('/users', (req,res) => {
	res.send(users)
})


app.delete('/delete-user', (req,res) => {
	let deleteUser = users.find((user) => {
		return user.username === req.body.username && user.password === req.body.password
	});

	if (deleteUser !== undefined) {
		let deleteIndex = users.findIndex((user) => {
			return user.username === deleteUser.username
		})
		res.send(`deleted user ${users[deleteIndex].username}.`)
		users.splice(deleteIndex, 1);
		console.log(users)
	} else {
		res.send(`user deletion not done`)
		console.log(users)
	}
})



//listens to the port and returning the message in the terminal
app.listen(port, () => console.log(`The server is running at port ${port}`))


